UltimakerApp
============

#### Prerequisites
* [NodeJS](http://nodejs.org/) and [Node Package Manager (npm)](https://www.npmjs.com/)
```
sudo apt-get install npm
```
* [Bower](http://bower.io/)
```
sudo npm -g install bower
```
* [gulpjs](http://gulpjs.com)
```
sudo npm -g install gulp
```
* [Cordova](http://cordova.apache.org/)
```
sudo npm -g install cordova
```
* [Phonegap](http://phonegap.com/)
```
sudo npm -g install phonegap
```
* [Android SDK](http://developer.android.com/)
Use Android SDK Manager to install
- Android SDK Tools
- Android SDK Platform-tools
- Android SDK Build-tools API 19.1
- Android 4.4.2 (API 19) SDK Platform

* [XCode / iOS SDK](https://developer.apple.com/xcode/downloads/)

#### Building
Clone this repository from GitHub, enter its folder and run the install script.
```
git clone git@github.com:Doodle3D/UltimakerApp.git
cd UltimakerApp
./install
```

#### Running
Gulp generates the files in `www/` and `platforms/`
```
gulp              # gulp keeps running and watches changed files
```

#### Install the app on your device:
```
phonegap run ios
phonegap run android
```
On Android you need to enable Developer options and USB debugging. 

You can also use the developer app from http://app.phonegap.com and let phonegap serve the files:

```
phonegap serve
```

-------------

#### Using the FakePrinter tool
The `/server/` folder in this repo contains a node file called `fakeprinter.js`. This file connects to the CloudServer and acts like a printer. It's a very limited implementation of the API but can be really useful for testing the UltimakerApp.
To use `fakeprinter.js` make sure to run `npm install` once in the `/server/` folder to install the node modules. Then run `node fakeprinter.js`. Check the source code for some keyboard shortcuts like `p` for setting the printer state to 'print' and `q` for setting the printer state to 'idle'.
