'use strict';

function assertType(variable,typename) {
  throw new Error("assertType is depricated. Use assert(arguments,['type',...],...)");
}

function assert(a,b) {
  var a = Array.prototype.slice.call(a);
  var c = Array.prototype.slice.call(arguments,2).join('.');
  for (var i in b) if (typeof a[i]!=b[i]) throw new TypeError(c+" argument["+i+"] should be "+b[i]+" ");
}

//////////////////////////////////////////////////////////////////////////////
// Phonegap

var phonegap_app = {
  initialize: function() {
    this.bindEvents();
  },

  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },

  onDeviceReady: function() {
    console.log("onDeviceReady");
    console.log('device',device.cordova,device.model,device.platform,device.uuid,device.version);

    window.plugins.webviewcolor.change('#F1EFF0');

    setTimeout(function() {
      navigator.splashscreen.hide();
    }, 2000);

    document.addEventListener("resume", phonegap_app.onResume, false);
    document.addEventListener("online", phonegap_app.onOnline, false);
    document.addEventListener("offline", phonegap_app.onOffline, false);

    console.log("onDeviceReady: navigator.connection");
    console.log(navigator.connection);
  },

  broadcast: function(msg) {
    console.log("app.broadcast",msg)
    var elem = angular.element(document.querySelector('[ng-controller]'));
    if (elem) elem.scope().$broadcast(msg); //FIXME, check why on android elem seems to be undefined
  },

  onOnline: function() {
    console.log("app.onOnline");
    phonegap_app.broadcast("onOnline");
  },
  
  onOffline: function() {
    console.log("app.onOffline");
    phonegap_app.broadcast("onOffline");
  },

  onResume: function() {
    console.log("app.onResume");
    phonegap_app.broadcast("onResume");
  },

};

phonegap_app.initialize();

//////////////////////////////////////////////////////////////////////////////
// Angular

// var useLocalServers = false;

localStorage.setItem("filename","");

var app = angular.module('UltimakerApp', ['ngTouch',  'ngRoute',  'ngAnimate', 'duScroll' ]);

// app.value("socketAccessPointURL","http://ultimaker.local:5000")
app.value("socketAccessPointURL","http://192.168.0.1:5000")
app.value("socketAccessPointURL","http://localhost:8081")
app.value("socketserverURL","https://cloud.doodle3d.com:443")
app.value("registerURL","https://cloud.doodle3d.com:443/user/register")
app.value("rootURL","http://um/")
app.value("deviceName","mobile device")

app.directive('myScroll', function($rootScope, $anchorScroll) {
    return function(scope, element) {
        $rootScope.$on('$routeChangeStart', function() {
           $anchorScroll(); 
        });
    };
});

app.config(['$routeProvider', function ($routeProvider) {
  var routeProvider = $routeProvider;

  function route(route,folder,view,controller) {
    if (!controller) controller=view; //use name of view as default controller
    routeProvider.when(route, {templateUrl:"views/"+folder+'/'+view+".html", controller:controller});
  }

  route('/', 'main','Home');
  route('/file/:url*','file','File','File');
  route('/file-printer','file','FilePrinter');
  route('/file-material','file','FileMaterial');
  route('/file-profile','file','FileProfile');
  route('/printer/:id','printer','Printer');
  route('/printersettings/:id','printer','PrinterSettings');
  route('/installprinter/:id','install','InstallPrinter');
  route('/printer-installed/:id', 'install','PrinterInstalled');
  route('/uninstallprinter/:id', 'install','UninstallPrinter');
  route('/localprinters', 'network','LocalPrinters');
  route('/searchnetworks', 'network','SearchNetworks');
  route('/join-secure-network/', 'network','JoinSecureNetwork');
  route('/join-success/:id', 'network','JoinSuccess');
  route('/connecting/', 'network','Connecting');
  route('/connecting-failed', 'network','ConnectingFailed');
  route('/addprinter', 'network','AddPrinter');
  route('/newprinter', 'network','NewPrinter');
  route('/about','main', 'About', 'StaticPage');
  route('/support','main', 'Support','StaticPage');

}]);

function handleOpenURL(url) {
  setTimeout(function() {
    console.log("app.handleOpenURL",url);
    //get your angular element
    var elem = angular.element(document.querySelector('[ng-controller]'));
    //get the injector.
    var injector = elem.injector();
    //get the service.
    var FileOpenService = injector.get('FileOpenService');
    //update the service.
    FileOpenService.url = url;
    FileOpenService.broadcast();
  }, 500);
}
