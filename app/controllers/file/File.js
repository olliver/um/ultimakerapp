app.controller('File', function($scope, Page, $routeParams, $rootScope, FileOpenService, $http, dataService, ConfigService, CloudService, PrinterRootService, PrinterDataService, socketFactory, InstalledPrintersService, ConfigService, PrinterPrinterService) {
    
  var className = "File";
  
  $scope.settings = {
    ultiviewerSupported: false
  };

  $scope.Page = Page;
  Page.setBackbutton(true);
  Page.setLogo(false);
  Page.setTitle('');
  Page.setSettingsbutton(false);

  CloudService.connect(init);

  function init() {
    $scope.url = $routeParams.url;
    $scope.filename = getFilename($scope.url);
    console.log(className,'url',$scope.url);

    var match = $scope.url.match(/file\/(\d+)/);
    if (match) loadPreviewFromYoumagine(match);

    var printerId = dataService.get('selectedPrinterId');
    var printer;

    console.log("printerId",printerId);

    if (printerId) {
      var p = PrinterDataService.getPrinterById(printerId);
      if (p.installed) printer = $scope.printer = p;
      else console.log(className,"printer",p.id,"not installed");
      
      if (printer) {
        PrinterRootService.connect(printer);
        PrinterPrinterService.connect(printer);
      }
      
    } else {
      console.log(className,"printer not defined");
    }
  }

  function loadPreviewFromYoumagine(match) {
    if (!match) return;
    var id = match[1];

    //if URL exists in dataService (request by id) use that, otherwise request the URL using API.
    if (dataService.get(id)) {
      $scope.imageSrc = dataService.get(id);
      $rootScope.safeApply();
    } else {
      //request image preview URL using temporary API
      $http.get("http://dev.companje.nl/youmagine/?id="+id).
      success(function(data, status, headers, config) {
        console.log(className,data);

        var imageId = data;
        var url = "https://www.youmagine.com/images/"+imageId+".json";

        $http.get(url).
        success(function(data, status, headers, config) {
          dataService.set(id,data.file.medium.url);
          $scope.imageSrc = data.file.medium.url;
          $rootScope.safeApply();
        }).
        error(function(data, status, headers, config) {
          console.log(className,"error 2",data);
        });
    
      }).
      error(function(data, status, headers, config) {
        console.log(className,"error 1",data);
      });
    }
  }

  function getFilename(url) {
    return url.split('/').pop();
  }

  ConfigService.connect(function() {
    $scope.material = ConfigService.getSelectedMaterial();
    $scope.profile = ConfigService.getSelectedProfile();
    $rootScope.safeApply();
    console.log("ConfigService.connect",$scope.material,$scope.profile)
  });
 
  $scope.rotate = function(axis,angle) {
    // setTimeout(function() {
    //       var ultiViewer = document.querySelector('ulti-viewer');
    //       ultiViewer.rotateSelection('y',90);
    //       console.log('rotated')
    //     },10000)

    // var ultiviewer = $('ulti-viewer');
    // console.log(className,'rotate',ultiviewer,axis,angle);
    
    // if (ultiviewer) ultiviewer.rotateSelection(axis,angle);
  }

  $scope.onImageLoaded = function() {
    $("#loader").hide();
  }

  $scope.printFile = function() {
    if (!$scope.printer.idle) return; //somehow the print button is still clickable even when it's disabled

    if (!confirm("Are you sure you want to start the print now?")) return;

    var settings = {
      url: $scope.url,
      filename: $scope.filename,
      material: $scope.material.id,
      profile: $scope.profile.id
    }

    console.log(className,'printFile',$scope.printer,$scope.filename,settings);

    PrinterDataService.updatePrinterData($scope.printer,{filename:$scope.filename});

    PrinterPrinterService.print($scope.printer,settings,function(err,data) {
      console.log("File.js print cb",err,data);
      $rootScope.go('/printer/'+$scope.printer.id);
    });
  }

  $scope.$on('$destroy', function (event) {

  });

});
