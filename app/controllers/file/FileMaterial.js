app.controller("FileMaterial", function($scope, Page, $rootScope, dataService, InstalledPrintersService, ConfigService) {
    $scope.Page = Page;
    Page.setBackbutton(true);
    Page.setLogo(false);
    Page.setTitle('');
    Page.setSettingsbutton(false);

    ConfigService.connect(function() {
        $scope.materials = ConfigService.getMaterials();
        console.log("FileMaterial ConfigService connect cb",$scope.materials);
        $rootScope.safeApply();
    })

    $scope.selectMaterial = function(id) {
        ConfigService.setSelectedMaterialById(id);
        $rootScope.back();
    }
})