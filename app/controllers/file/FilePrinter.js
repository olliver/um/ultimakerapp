app.controller("FilePrinter", function($scope, Page, $rootScope, dataService, InstalledPrintersService, CloudService, ConnectionService) {
  var className = "FilePrinter";

  $scope.Page = Page;
  $scope.printers = InstalledPrintersService.getPrinters();
  $scope.connection = ConnectionService.connect();
  $scope.cloud = CloudService.connect(init);

  function init() {
    console.log(className,'init');
  }

  $scope.selectPrinter = function(printer) {
    console.log("selectPrinter",printer);
    dataService.save("selectedPrinterId",printer.id);
    $rootScope.back();
  }

  $scope.$on('$locationChangeStart', function(event) {
    console.log(className,'$locationChangeStart');
    InstalledPrintersService.disconnect();
    ConnectionService.disconnect();
    AccessPointService.disconnect();
  });

})