app.controller("FileProfile", function($scope, Page, $rootScope, dataService, InstalledPrintersService,ConfigService) {
    var className = "FileProfile";

    $scope.Page = Page;
    Page.setBackbutton(true);
    Page.setLogo(false);
    Page.setTitle('');
    Page.setSettingsbutton(false);

    ConfigService.connect(function() {
        $scope.profiles = ConfigService.getProfiles();
        console.log("FileProfile ConfigService connect cb",$scope.profiles);
        $rootScope.safeApply();
    })

    $scope.selectProfile = function(id) {
        console.log(className,"selectProfile",id);
        ConfigService.setSelectedProfileById(id);
        $rootScope.back();
    }

})