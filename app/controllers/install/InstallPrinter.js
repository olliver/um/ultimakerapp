app.controller("InstallPrinter", function($scope, Page, $rootScope, $routeParams, socketFactory, $location, PrinterDataService, CloudService, dataService, InstalledPrintersService, LocalPrintersService) {

  var className = "InstallPrinter";
  $scope.Page = Page;
  Page.setBackbutton(true);
  Page.setLogo(false);
  Page.setTitle('');
  Page.setSettingsbutton(false);

  $scope.connected = false;

  console.log("InstallPrinter",$routeParams.id);

  var printer = $scope.printer = PrinterDataService.getPrinterById($routeParams.id);

  $scope.newName = printer.name;

  CloudService.connect(init);

  function init() {
    console.log(className,'init',printer);

  }

  $scope.installPrinter = function(newName) {
    console.log(className,'installPrinter',printer);

    PrinterDataService.updatePrinterData(printer,{customName:newName});

    InstalledPrintersService.install(printer, function() {
      console.log(className,'installPrinter cb');
      // PrinterDataService.updatePrinterData(printer,{installed:true});
      $rootScope.go('/printer-installed/'+printer.id);
      $rootScope.$apply();
    });

   
  };

});
