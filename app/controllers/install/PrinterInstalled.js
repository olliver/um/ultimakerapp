app.controller("PrinterInstalled", function($scope, Page, $rootScope, $routeParams, $location, dataService) {
    $scope.Page = Page;

    Page.setTitle('');
    Page.setBackbutton(false);
    Page.setLogo(false);
    Page.setSettingsbutton(false);
        
    $scope.printer = {
        id: $routeParams.id
    };
        
});
