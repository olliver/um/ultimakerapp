app.controller("UninstallPrinter", function($scope, Page, $rootScope, $routeParams, $location, CloudService, InstalledPrintersService, PrinterDataService) {

  var className = 'UninstallPrinter';

  $scope.Page = Page;
  Page.setTitle(''); 
  Page.setBackbutton(true);
  Page.setLogo(false);
  Page.setSettingsbutton(false);

  var printer = $scope.printer = PrinterDataService.getPrinterById($routeParams.id);

  CloudService.connect(init);

  function init() {
    console.log(className,'printer',printer);
  }

  $scope.uninstallPrinter = function() {

    InstalledPrintersService.uninstall(printer, function() {
      console.log(className,'uninstallPrinter cb');
      PrinterDataService.updatePrinterData(printer,{installed:false});
      $rootScope.go('/');
      $rootScope.safeApply();
    });

  }


});
