app.controller("AppPage", function($routeParams, $scope, $rootScope, Page) {
  var className = "AppPage";
  $scope.Page = Page;

  console.log(className,"init");
  var hasWebGL = !!window.WebGLRenderingContext && !!document.createElement("canvas").getContext("webgl");
  console.log(className,"hasWebGL",hasWebGL);
  



  $scope.gotoPrinterSettings = function() {
    console.log(className,"gotoPrinterSettings");
    $rootScope.go('/printersettings/'+$routeParams.id);
  }
})