app.controller("Home", function($scope, Page, $rootScope, $location, CloudService, FileOpenService, dataService, InstalledPrintersService, PrinterRootService, socketFactory, ConnectionService) {
  var className = "Home";

  $scope.Page = Page;
  Page.setTitle('');
  Page.setBackbutton(false);
  Page.setLogo(true);
  Page.setSettingsbutton(false);
  
  $scope.printers = InstalledPrintersService.getPrinters();
  $scope.connected = false;
  $scope.cloud = CloudService.connect(init);
  $scope.connection = ConnectionService.getConnection();
  ConnectionService.connect();

  function init() {
    console.log(className,'init');

    $scope.connected = true;
    $rootScope.safeApply(); //needed to update to 'connected' state
   
    $rootScope.$on('PrinterOnline', function(event, printer) {
      console.log(className,'PrinterOnline event');
      PrinterRootService.connect(printer); //,function() { onPrinterRootServiceConnected(printer); });
    });

    $rootScope.$on('PrinterOffline', function(event, printer) {
      console.log(className,'PrinterOffline event');
      PrinterRootService.disconnect(printer);
    });   

  }

  $scope.openPrinter = function(printer) {
    console.log(className,'openPrinter',printer.id);
    $rootScope.go('/printer/'+printer.id);
    $rootScope.safeApply();
  }

  $scope.gotoYouMagine = function() {
    console.log(className,'gotoYouMagine');

    //when inAppBrowser does not work, check if cordova.js is loaded and if the plugins are part of the xcode project
    var ref = window.open('http://youmagine.com/designs', '_blank', 'location=yes,transitionstyle=fliphorizontal');
    ref.addEventListener('loadstart', function(event) {

        if (event.url.toLowerCase().indexOf(".stl")>0) {
            ref.close();

            setTimeout(function() {
                // FileOpenService.url = event.url;
                $rootScope.go('/file/'+event.url);
                $rootScope.safeApply(); //$apply();
            },500);
        } else if (event.url.toLowerCase().indexOf(".zip")>0) {
          alert('Oops, you selected a .ZIP file. Currently only .STL files are supported by the Ultimaker App. Please scroll down to Documents and select an STL file.');
        }
    });
  }

  $scope.openTestWebsite = function(url) {
    var ref = window.open(url, '_blank', 'location=yes,transitionstyle=fliphorizontal');
    ref.addEventListener('loadstart', function(event) {
        if (event.url.indexOf(".stl")>0) {
            ref.close();

            setTimeout(function() {
                // FileOpenService.url = event.url;
                $rootScope.go('/file/'+event.url);
                $rootScope.safeApply(); //$apply();
            },200);
        }
    });
  };

  $scope.openTestFile = function(url) {
    $rootScope.go('/file/'+url);
    $rootScope.safeApply();
  };

  $scope.clearLocalStorage = function() {
    if (confirm("Are you sure you want to clear localStorage?")) {
      for (var i in localStorage) {
        localStorage.removeItem(i);
      }
    }
  }

  $scope.$on('$locationChangeStart', function(event) {
    socketFactory.disconnectAll();
    ConnectionService.disconnect();
  });

});


