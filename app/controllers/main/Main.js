app.controller('Main', function ($scope, $rootScope, $window, $location, $timeout, $document) {
  var className = "Main";
  $scope.slide = '';

  $rootScope.home = function() {
    console.log(className,"home()");
    $location.url("/");
  }

  $rootScope.back = function() {
    $scope.slide = 'slide-right';
    $rootScope.safeApply();
    // $document.scrollTop(0, 250).then(function() {
      $window.history.back();
      $rootScope.safeApply();
    // });
  }

  $rootScope.go = function(path) {
    console.log(className,"go",path);
    $scope.slide = 'slide-left';
    // $rootScope.safeApply();
    $location.url(path);
    $rootScope.safeApply();
    // $document.scrollTop(0, 250).then(function() {
      // console.log(className,"go after scroll",path);
    // });
  }

  $rootScope.safeApply = function() {
    $timeout(function() {
      $scope.$apply();
    })
  }

  $rootScope.alert = function(text) {
    alert(text);
  }

  $scope.$on('handleOpenURL', function() {
    console.log(FileOpenService.url);
    $rootScope.go('/file?'+Math.random());
  });

})