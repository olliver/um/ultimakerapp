app.controller("AddPrinter", function($scope, Page, $rootScope, $routeParams, socketAccessPointURL, AccessPointService) {
  var className = "AddPrinter";
  var interval;

  $scope.Page = Page;
  Page.setBackbutton(true);
  Page.setLogo(false);
  Page.setTitle('');
  Page.setSettingsbutton(false);
  
  console.log(className,"init");
  console.log("routeParams",$routeParams);

  if (navigator.wifi) {

    interval = setInterval(function() {
      navigator.wifi.getWifiInfo(function(data) {
        AccessPointService.getData().suggestedNetwork = data.connection.SSID;
        AccessPointService.getData().currentNetwork = data.connection.SSID;
        AccessPointService.getData().currentIpAddress = data.connection.IpAddress;
        console.log(data.connection.SSID);
        console.log(data.connection.IpAddress);
      });
    },1000);

  }  

  $scope.$on('onResume', function (event) {
    console.log("AddPrinter.onResume",AccessPointService.getData().suggestedNetwork);

    ///TODO: MOVE ZeroConf stuff to ZeroConfService.js

    //CHECK ME

    ZeroConf.watch("_3dprinter._tcp.local.", function(result) {
       console.log('>>>>>>>>>>>>>>>>>');
       console.log(JSON.stringify(result, null, 4));

       if (result.service.name==AccessPointService.getData().currentNetwork) {
         console.log("YES!");

         AccessPointService.getData().url = "http://"+result.service.server+":5000";

         AccessPointService.connect(function() {
          console.log(className,"connected");
          $rootScope.go("/newprinter");
        });
       }

       //console.log(util.inspect(service, false, null));
    });

    // navigator.wifi.getWifiInfo(function(data) {
    //   console.log(data.connection.SSID);
    // });

  });

  $scope.$on('$locationChangeStart', function (event) { //$destroy
    console.log(className,"locationChangeStart");
    AccessPointService.disconnect();
    clearInterval(interval);
    interval = undefined;
  });

});
