app.controller("Connecting", function($scope, Page, $rootScope, $routeParams, deviceName, AccessPointService, LocalPrintersService, socketFactory) {
    var className = "Connecting";

    Page.setBackbutton(true);
    Page.setLogo(false);
    Page.setTitle('');
    Page.setSettingsbutton(false);
        
    $scope.printer = {
        ssid: AccessPointService.getData().ssid,
        name: AccessPointService.getData().accesspointName,
    }

    LocalPrintersService.connect(function() {
        console.log(className,'connected to LocalPrintersService');

        LocalPrintersService.waitForPrinterByName($scope.printer.name, function(printer) {
            console.log(className,"waitForPrinterByName",printer.name,"found");
            $rootScope.go("join-success/" + printer.id);
        });
    });

    $scope.$on('$destroy', function(event) {
        AccessPointService.disconnect();
        // LocalPrintersService.disconnect();
    });    
    
})
