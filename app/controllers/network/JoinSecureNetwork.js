app.controller("JoinSecureNetwork", function($scope, Page, $routeParams, socketFactory, $rootScope, AccessPointService) {
    var className = "JoinSecureNetwork";

    Page.setBackbutton(true);
    Page.setLogo(false);
    Page.setTitle('');
    Page.setSettingsbutton(false);

    console.log("JoinSecureNetwork.init");

    $scope.ssid = AccessPointService.getData().ssid;   //$routeParams.ssid;

    $scope.next = function() {
        AccessPointService.joinNetwork($scope.ssid,$scope.password);
        $rootScope.go('/connecting'); ///'+$scope.ssid);
    };
    
    
});
