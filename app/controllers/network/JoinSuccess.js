app.controller("JoinSuccess", function($scope, Page, $rootScope, $routeParams, socketAccessPointURL) {
    // Page.setTitle('Joined!');
    Page.setBackbutton(false);
    Page.setLogo(false);
    Page.setTitle('');
    Page.setSettingsbutton(false);
    
    $scope.printer = {
        id: $routeParams.id,
    };
    
    console.log("JoinSuccess.init");

    $scope.next = function() {
        $rootScope.go('/installprinter/'+$scope.printer.id); 
    };
    
})
