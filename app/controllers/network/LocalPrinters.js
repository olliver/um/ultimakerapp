app.controller('LocalPrinters', function($scope, Page, $routeParams, socketFactory, $rootScope, CloudService, LocalPrintersService, InstalledPrintersService, ConnectionService, ZeroConfService) {
  var className = "LocalPrinters";
  console.log(className);

  Page.setTitle('');
  Page.setBackbutton(true);
  Page.setLogo(false);
  Page.setSettingsbutton(false);

  // $scope.zeroconfPrinters = ZeroConfService.getPrinters();
  $scope.localPrinters = LocalPrintersService.getPrinters();
  $scope.connection = ConnectionService.connect();
  $scope.cloud = CloudService.connect(init);

  // ZeroConfService.connect();

  function init() {
    console.log(className,'CloudService completed',$scope.cloud);
    // $scope.connected = true;
    // $rootScope.safeApply(); //needed to update to 'connected' state

    LocalPrintersService.connect(function() {
      console.log("LocalPrintersService connect");
    });
  }
  
  $scope.openLocalPrinter = function(printer) {
    console.log(className,"openLocalPrinter",printer,"installed:",printer.installed);

    if (!printer.installed) { //this flag is set by InstalledPrinters onAppear and removed onDisappear
      $rootScope.go('/installprinter/'+printer.id);
    } else {
      $rootScope.go('/printer/'+printer.id);
    }
  }

  $scope.$on('$locationChangeStart', function(event) { //'$destroy'
    socketFactory.disconnectAll();
    ConnectionService.disconnect();
  });

});