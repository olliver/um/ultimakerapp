app.controller("NewPrinter", function($scope, Page, $routeParams, $rootScope, socketAccessPointURL, AccessPointService) {
    var className = "NewPrinter";

    Page.setTitle('');
    Page.setBackbutton(false);
    Page.setLogo(false);
    Page.setSettingsbutton(false);

    AccessPointService.connect();
    
    $scope.data = AccessPointService.getData();

    $scope.next = function(event) {
      $rootScope.go('/searchnetworks');
    };
    
    $scope.$on('$destroy', function (event) {
       AccessPointService.disconnect();
    });

})
