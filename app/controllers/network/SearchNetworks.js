app.controller("SearchNetworks", function($scope, Page, $rootScope, socketAccessPointURL,AccessPointService) {
    var className = "SearchNetworks";

    Page.setTitle('');
    Page.setBackbutton(true);
    Page.setLogo(false);
    Page.setSettingsbutton(false);

    AccessPointService.connect();

    $scope.data = AccessPointService.getData();

    $scope.next = function(network) {
        console.log(className,"next",network);
        
        if (network.security[0]!='none') {
            $rootScope.go('/join-secure-network'); ///' + network.ssid);
        } else {
            console.log("emit joinNetwork");
            AccessPointService.joinNetwork(network.ssid);
            $rootScope.go('/connecting'); ///'+network.ssid);
        }

    };

   $scope.$on('$destroy', function(event) {
       AccessPointService.disconnect();
   }); 
})
