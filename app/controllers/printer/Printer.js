app.controller("Printer", function($scope, Page, $rootScope, $routeParams, CloudService, PrinterDataService,ConnectionService, dataService, InstalledPrintersService, PrinterWebcamService, PrinterPrinterService, socketFactory, PrinterRootService, PrinterSlicerService, LocalPrintersService) {
  var className = "Printer";

  $scope.Page = Page;
  Page.setTitle('');
  Page.setBackbutton(true);
  Page.setLogo(false);
  Page.setSettingsbutton(true);
  // $scope.imageSrc = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7";
  $scope.imageReady = false;
  $scope.safeApply();

  var printer = $scope.printer = PrinterDataService.getPrinterById($routeParams.id);

  console.log(className,'before init',printer);

  ConnectionService.connect();
  CloudService.connect(init);

  function init() {
    console.log(className,'printer',printer);
    LocalPrintersService.connect(); 
    PrinterRootService.connect(printer);
    PrinterPrinterService.connect(printer);
    PrinterSlicerService.connect(printer); 

    $scope.localStreamUrl = "http://" + printer.name + ".local:8080/?action=stream";

    $scope.imageSrc = $scope.localStreamUrl;

    console.log(className,$scope.imageSrc);
  }

  $scope.formatPercentage = function(p) {
    return Math.round(p*100*1000)/1000;
  }

  $scope.onStreamLoadError = function() {
    console.log(className,"onStreamLoadError, now connect to PrinterWebcamService");
    PrinterWebcamService.connect(printer);
    $scope.imageReady = false;
  }

  $scope.onWebcamLoaded = function() {
    console.log(className,'onWebcamLoaded');
    $scope.imageReady = true;
  }

  $rootScope.$on('onConnectionStateChanged',function(data) {
    console.log(className,'onConnectionStateChanged',data);
  });

  $rootScope.$on('onPrinterDataUpdate',function(event,p) {
    //console.log(className,'onPrinterDataUpdate',p,p==printer);
    // if (p==printer && p.root && p.root.networkAddress) {
    //   // console.log(className,'printer onPrinterDataUpdate local:',printer.isLocal);

    //   if (printer.isLocal) {
    //     PrinterWebcamService.disconnect(printer);
    //     $scope.imageSrc = "http://" + printer.root.networkAddress.address + ":8080/?action=stream";
    //   } else {
    //     console.log(className,'!isLocal connect to PrinterWebcamService');
    //     PrinterWebcamService.connect(printer);
    //   }
    // }
  });

  $rootScope.$on('onWebcamImage',function(event,data) {
    console.log(className,"onWebcamImage");
    $scope.imageSrc = data;
    $rootScope.safeApply();
    $scope.imageReady = true;
  });
     
  $scope.stopPrint = function() {
    if (confirm("Are you sure you want to ABORT the current print?")) {
      console.log(className,'stopPrint')
      PrinterPrinterService.stopPrint(printer);
    }
  }

  $scope.$on('$locationChangeStart', function(event) { //'$destroy'
    PrinterRootService.disconnect(printer);
    PrinterPrinterService.disconnect(printer);
    PrinterWebcamService.disconnect(printer);
    PrinterSlicerService.disconnect(printer);
    LocalPrintersService.disconnect();
    ConnectionService.disconnect();
  });

  $scope.showPrinterInfo = function() {
    console.log(printer);
  }

});
