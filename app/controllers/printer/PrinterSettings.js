app.controller("PrinterSettings", function($scope, Page, $rootScope, $routeParams, PrinterNetworkService,CloudService,dataService,InstalledPrintersService,PrinterDataService,PrinterRootService,PrinterPrinterService) {

  var className = "Printer";

  $scope.Page = Page;
  Page.setTitle('');
  Page.setBackbutton(true);
  Page.setLogo(false);
  Page.setSettingsbutton(false);

  var printer = $scope.printer = PrinterDataService.getPrinterById($routeParams.id);

  CloudService.connect(init);

  function init() {
    console.log(className,'printer',printer);
    PrinterRootService.connect(printer);
    PrinterPrinterService.connect(printer);
    PrinterNetworkService.connect(printer);
    $scope.newName = printer.customName;
    $scope.newType = printer.type;
  }


  $scope.saveSettings = function(newName) {
    console.log(className,"saveSettings",newName);    
    PrinterDataService.updatePrinterData(printer,{customName:newName});
    $rootScope.back();
  };

  $scope.updatePrinterType = function(type) {
    console.log(className,"updatePrinterType",type);    
    PrinterDataService.updatePrinterData(printer,{type:type});
    $rootScope.back();
  };

  $scope.forgetNetwork = function() {
    PrinterNetworkService.forgetNetwork(printer);
    $rootScope.go('/');
  }

  $scope.home = function() {
    console.log(className,'homePrinter');
    PrinterPrinterService.home(printer);
  }

  $scope.gotoUninstallPrinter = function() {
    $rootScope.go('uninstallprinter/'+$scope.printer.id);
  };

});
