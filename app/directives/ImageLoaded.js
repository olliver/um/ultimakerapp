app.directive('rcImageLoaded', function() {
  return {
    restrict: 'A',

    link: function(scope, element, attrs) {
      element.bind('load', function() {
        scope.$apply(attrs.rcImageLoaded);
      });
    }
  }
});

app.directive('rcImageLoadError', function() {
  return {
    restrict: 'A',

    link: function(scope, element, attrs) {
      element.bind('error', function() {
        scope.$apply(attrs.rcImageLoadError);
      });
    }
  }
});
