app.directive('rcItem', function() {
  return {
    restrict: 'C',
    replace: true,
    transclude: true,
    scope: {
      title: '@',
      subtitle: '@',
      iconUrl: '@',
      iconClass: '@',
      transclude: '@',
    },
    templateUrl: 'views/widgets/Item.html'
  }
});

app.directive('rcPrinter', function() {

  return {
    restrict: 'EAC',
    replace: true,
    transclude: true,
    // scope: {
    //   icon: '@',
    //   title: '@',
    //   subtitle: '@'
    // },
    templateUrl: 'views/widgets/Printer.html',
  };

});

app.directive('rcWeblink', function() {

  return {
    restrict: 'EAC',
    replace: true,
    transclude: true,
    scope: {
      icon: '@',
      title: '@',
      subtitle: '@'
    },
    templateUrl: 'views/widgets/Weblink.html', // BE CAREFUL: case-sensitive on mobile devices!
  };

});
