app.directive('rcPanel', function() {

  return {
    restrict: 'EAC',
    replace: true,
    transclude: true,
    scope: {
      title: '@',
      subtitle: '@'
    },
    templateUrl: 'views/widgets/Panel.html',
  };

});
