app.service('CloudService', function($rootScope,dataService,AuthFactory,InstalledPrintersService,socketFactory) {
  var className = "CloudService";

  var data = {
    connected: false
  }

  console.log(className,"construct")

  // setInterval(function() {
  //   console.log('toggle');
  //   data.connected = !data.connected;
  //   $rootScope.safeApply();
  // },2000);

  $rootScope.$on("onRootSocketConnect",function() {
    data.connected = true;
  });

  $rootScope.$on("onRootSocketConnectError",function(data) {
    console.log(className,'onRootSocketConnectError',data);
    data.connected = false;
    data.errorType = data.type;
  });

  function connect(cb) {

    AuthFactory.validate(function() {

      console.log(className,'key validated')

      InstalledPrintersService.connect(function() {

        console.log(className, 'connected to /printers');

        data.connected = true;

        if (cb) cb();

      });

    }, function() {
      console.error(className,"key validation error"); 
    });

    return data; //CHECK ME

  }

  function getData() {
    return data;
  }

  return {
    getData: getData,
    connect: connect,
  }
});