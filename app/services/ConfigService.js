app.service('ConfigService', function(socketFactory, $rootScope, dataService) {
  var className = 'ConfigService';
  var connected  = false;
  var configSocket;
  var slicers;

  function connect(cb) {
    console.log(className,'connect');

    if (connected) {
      console.log("already connected");
      if (cb) cb();
      return;
    }

    configSocket = socketFactory.connect('config')

    .on('connect', function() {

      getSlicers(function(err,data) {
        console.log(className,'getSlicers:cb:',err,data)
        slicers = data;

        connected = true;
        if (cb) cb();
      })
    })
  }

  function getSlicers(cb) {
    console.log(className,'getSlicers');

    configSocket.emit('getSlicers', function(err,data) {
      console.log(className,'getSlicers cb err/data: ',err,data);
      if (cb) cb(err,data);
    });
  }

  function getSelectedMaterial() {
    var id = dataService.get("selectedMaterial");
    console.log(className,"getSelectedMaterial",id);
    if (id) return { id:id, title:slicers.curaEngine.material[id]};
  }

  function getSelectedProfile() {
    var id = dataService.get("selectedProfile");
    console.log(className,"getSelectedProfile",id);
    if (id) return { id:id, title:slicers.curaEngine.profile[id]};
  }

  function setSelectedProfileById(id) {
    dataService.save("selectedProfile",id);
  }

  function setSelectedMaterialById(id) {
    dataService.save("selectedMaterial",id);
  }

  function convertToArray(dict) {
    if (!dict) return [];

    var items = [];
    for (id in dict) {
      items.push({id:id, title:dict[id]});
    }
    return items; //returns array with {id,title} objects
  }

  function getProfiles() {
    if (!slicers) return;
    return convertToArray(slicers.curaEngine.profile);
  }

  function getMaterials() {
    if (!slicers) return;
    return convertToArray(slicers.curaEngine.material);
  }

  return {
    connect: connect,
    getMaterials: getMaterials,
    getProfiles: getProfiles,
    getSelectedMaterial: getSelectedMaterial,
    getSelectedProfile: getSelectedProfile,
    setSelectedMaterialById: setSelectedMaterialById,
    setSelectedProfileById: setSelectedProfileById
  }

});
