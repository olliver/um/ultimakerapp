app.service('ConnectionService', function(socketFactory, $rootScope, dataService) {
  var className = "ConnectionService";
  var interval;

  var connection = {
    type: "",
    online: true,
    wifi: {
      ssid: "",
      connected: true
    }
  }

  function connect() {
    console.log(className,"connect");

    setTimeout(update,1000);

    //DISABLE UPDATE
    // if (!interval) interval = setInterval(update,5000);
    // else console.log(className,"Warning: already connected. Disconnect from services first");
    
    return connection;
  }

  $rootScope.$on('onResume', function (event) {
    console.log(className,"onResume");
    update();
  })

  $rootScope.$on('onOnline', function (event) {
    console.log(className,"onOnline");
    connection.online = true;
    $rootScope.$broadcast('onConnectionStateChanged',connection);
  })

  $rootScope.$on('onOffline', function (event) {
    console.log(className,"onOffline");
    connection.online = false;
    $rootScope.$broadcast('onConnectionStateChanged',connection);
  })

  function update() {

    if (connection.online != navigator.onLine) {
      connection.online = navigator.onLine;
      // console.log(className,'update, onConnectionStateChanged',connection.online);
      $rootScope.$broadcast('onConnectionStateChanged',connection);
    }

    if (navigator.connection && navigator.wifi) {
      connection.type = navigator.connection.type;
      connection.wifi.connected = (navigator.connection.type == Connection.WIFI);
      // connection.online = (navigator.connection.type != Connection.NONE); //this works on mobile, not on desktop
      if (connection.wifi.connected) {
        navigator.wifi.getWifiInfo(function(result) {
          console.log(className,"on getWifiInfo");
          connection.wifi.ssid = result.connection.SSID;
        });
      }
    }

    $rootScope.safeApply();  
  }

  function getConnection() {
    return connection;
  }

  function disconnect() {
    console.log(className,'disconnect');
    
    //clearInterval(interval);
    //interval = undefined; //shows connect() that we're actually disconnected
  }

  return {
    getConnection: getConnection,
    connect: connect,
    disconnect: disconnect
  } 

});