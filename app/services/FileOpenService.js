app.factory('FileOpenService', function(){
  return {
         url: 'NO_URL',
         broadcast: function() {
           var elem = angular.element(document.querySelector('[ng-controller]'));
           elem.scope().$broadcast('handleOpenURL');
         }
  };
});