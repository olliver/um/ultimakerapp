app.service('PrinterDataService', function($rootScope,dataService) {

  var className = 'PrinterDataService';
  var printers = []; //hashtable/dictionary indexed on printer.id

  function getPrinters() {
    return printers;
  }

  function getPrinterById(id) {

    if (!printers[id]) {

      var obj = localStorage.getItem(id);
      
      if (!obj) {
        printers[id] = { id:id };
      } else {
        printers[id] = JSON.parse(obj);

        //some data should be ignored from localStorage, like states
        printers[id].isLocal = false;

      }

      console.log(className,printers[id]);
    }

    return printers[id];
  }

  function removePrinter(printer) {
    if (printer) {
      delete printers[printer.id];
    }
  }

  function merge(target, source) {
    // Merges two (or more) objects,  giving the last one precedence
    //https://gist.github.com/svlasov-gists/2383751

    if (typeof target !== 'object') {
      target = {};
    }
    
    for (var property in source) {        
      if (source.hasOwnProperty(property)) {
        var sourceProperty = source[ property ];
        if ( typeof sourceProperty === 'object' ) {
          target[ property ] = merge( target[ property ], sourceProperty );
          continue;
        }
        target[ property ] = sourceProperty;
      }
    }
    for (var a = 2, l = arguments.length; a < l; a++) {
      merge(target, arguments[a]);
    }

    return target;
  };

  function updatePrinterData(printer,data) {
    merge(printer,data);

    console.log(className,'updatePrinterData',printer.name,printer.type);

    ////////////////
    // computed values & default values
    if (!printer.customName) printer.customName = printer.name;
    if (!printer.type) printer.type = 'ultimaker2';

    printer.statusClass = getStatusClass(printer);
    printer.idle = getIsIdle(printer);
    printer.slicing = getIsSlicing(printer);
    printer.statusDescription = getStatusDescription(printer); 
    printer.printing = getIsPrinting(printer);
    printer.heating = getIsHeating(printer);
    printer.heatingBed = getIsHeating(printer) && printer.type=='ultimaker2';
    printer.stopping = getIsStopping(printer);
    /////////////////////

    localStorage.setItem(printer.id, JSON.stringify(printer));

    console.log(className,localStorage.getItem(printer.id));


    $rootScope.$broadcast('onPrinterDataUpdate',printer);

    $rootScope.safeApply(); //on one single place... but not ideal

    // console.log(className,'update',printer)
  }

  function getIsPrinting(printer) {
    if (!printer.online) return false;
    if (!printer.root) return false;
    if (!printer.root.printerState) return false;
    if (!printer.root.printerState.state) return false;
    if (printer.root.printerState.state!="printing") return false;
    if (getIsHeating(printer)) return false;
    return true;
  }

  function getIsStopping(printer) {
    if (!printer.online) return false;
    if (!printer.root) return false;
    if (!printer.root.printerState) return false;
    if (!printer.root.printerState.state) return false;
    if (printer.root.printerState.state!="stopping") return false;
    return true;
  }

  function getIsHeating(printer) {
    if (!printer.online) return false;
    if (!printer.printer) return false;
    if (!printer.printer.temperatures) return false;
    if (!printer.printer.temperatures.heating) return false;
    return true;
  }

  function getIsSlicing(printer) {
    if (!printer.online) return false;
    if (!printer.root) return false;
    if (!printer.root.slicerState) return false;
    if (!printer.root.slicerState.state) return false;
    if (printer.root.slicerState.state!="slicing") return false;
    return true;
  }

  function getIsIdle(printer) {
    if (!printer.online) return false;
    if (!printer.root) return false;
    if (!printer.root.printerState) return false;
    if (!printer.root.printerState.state) return false;
    if (printer.root.printerState.state!="idle") return false;
    if (getIsSlicing(printer)) return false; //or is this default?
    return true;
  }

  function getStatusClass(printer) {
    if (!printer.online) return 'isoffline';
    if (!printer.root) return 'isoffline';
    if (!printer.root.printerState) return 'isoffline';
    if (!printer.root.printerState.state) return 'isoffline';
    
    switch (printer.root.printerState.state) {
      case 'disconnected': return 'isoffline'; //= HostModule not connected to Arduino
      case 'connecting'  : return 'isoffline'; //= HostModule connecting to Arduino
      case 'idle'        : return 'isonline';
      case 'printing'    : return 'isprinting';
      case 'stopping'    : return 'isprinting';
      case 'unkown'      : return 'isoffline';
      default            : return 'isoffline';
    }
    return 'isoffline';
  }

  function getStatusDescription(printer) {
    if (!printer.online) return 'Offline';
    if (!printer.root) return 'Unknown';
    if (!printer.root.printerState) return 'Unknown';
    if (!printer.root.printerState.state) return 'Unknown';

    switch (printer.root.printerState.state) {
      case 'disconnected': return 'USB disconnected'; //= HostModule not connected to Arduino
      case 'connecting'  : return 'Connecting'; //= HostModule connecting to Arduino
      case 'idle'        : return 'Online';
      case 'printing'    : return 'Printing';
      case 'stopping'    : return 'Stopping';
      case 'unkown'      : return 'Unknown';
      default            : return 'Unknown';
    }
    return 'Unknown';
  }

  function loadItem(id,key,defaultValue) {
    return localStorage.getItem(id+key) || defaultValue;
  }

  function saveItem(id,key,value) {
    localStorage.setItem(id+key,value);
  }

  return {
    getPrinters: getPrinters,
    getPrinterById: getPrinterById,
    updatePrinterData: updatePrinterData,
    removePrinter: removePrinter,
  }

});