app.service('ZeroConfService', function(socketFactory, $rootScope, dataService) {
  var className = "ZeroConfService";

  var printers = [];

  function connect() {
    console.log(className,'connect');
  }

  function getPrinters() {
    return printers;
  }

  function disconnect() {
    console.log(className,'disconnect');
  }

  // function scan() {

  //   // if (!ZeroConf) {
  //   //   console.log('ZeroConf plugin not found');
  //   //   return;
  //   // }

  //   // ZeroConf.watch("_3dprinter._tcp.local.", function(result) {
  //   //   console.log(className,"found",result.service.hostName);
  //   // });

    
  //   //   // console.log('>>>>>>>>>>>>>>>>>');
  //   //   // console.log(JSON.stringify(result, null, 4));

  //   //   // if (result.service.name==AccessPointService.getData().currentNetwork) {
  //   //   //   console.log("YES!");

  //   //   //   AccessPointService.getData().url = "http://"+result.service.server+":5000";

  //   //   //   AccessPointService.connect(function() {
  //   //   //     console.log(className,"connected");
  //   //   //     $rootScope.go("/newprinter");
  //   //   //   });
  //   //   // }
  //   //    //console.log(util.inspect(service, false, null));
  //   //  });

  // }

  return {
    connect: connect,
    getPrinters: getPrinters,
    disconnect: disconnect,
  }

});
