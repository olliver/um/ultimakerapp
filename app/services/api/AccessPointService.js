app.service('AccessPointService', function(socketFactory, $rootScope, dataService, socketAccessPointURL) {
    var className = "AccessPointService";

    var data = {
        accesspointName: dataService.get("accesspointName"),
        ssid: dataService.get("ssid"),
        suggestedNetwork: dataService.get("suggestedNetwork"),
        ipaddress: dataService.get("ipaddress"),
        networks: []
    }

    var rootSocket;
    var networkSocket;
    var _nsps = [];

    function debug(msg) {
        console.log(className,msg);
    }

    function connectTo(nspName,opts,callback) {
      if(typeof opts == 'function') {
        callback = opts;
        opts = null;
      }
      opts = opts || {};
      opts.autoConnect = false;
      //var url = _config.cloudUrl + nspName; // + '?key=' + _key + '&type=printer';
      var url = nspName;

      debug('connecting: ' + url, opts || '');
      
      // ToDo: connect won't work when key changed...
      // using forceNew one works, but then the next (without forceNew) also fails
      var nsp = io(url,opts);
      _nsps.push(nsp);
      
      if(nsp.connected) {
        callback(null,nsp);
        return nsp;
      }

      // if not connected we explicitly reconnect 
      // (needed after manual disconnect)
      nsp.connect(); 
      debug(nspName+': waiting on events...');
      
      // Check connecting status. 
      // Currently if connecting fails we stop trying.
      function onConnect() {
        debug(nspName+': connect');
        callback(null,nsp);
      }
      
      nsp.once('connect',onConnect);
      return nsp;
    }

    function connect(cb) {
        
        var url = data.url || socketAccessPointURL;

        rootSocket = connectTo(url, function(err,socket) {

            if (cb) cb();

            socket.on("name", onName);
        });

        networkSocket = connectTo(url + "/network", function(err,socket) {

            socket.emit('getNetworks', function(err,res) {
                console.log(className,'getNetworks',res);

                data.networks = res.networks;

                for (var i in data.networks) {
                    data.networks[i].secured = data.networks[i].security[0]!='none';
                }

                $rootScope.safeApply();
            });


        });
    }

    function onName(res) {
        console.log(className,'onName',res.name);
        data.accesspointName = res.name;
        dataService.save("accesspointName",res.name);
        $rootScope.safeApply();
    }

     function disconnect() {
        debug(rootSocket);
        debug(networkSocket);

        if (rootSocket) {
          rootSocket.removeAllListeners();
          rootSocket.disconnect();
        }
        if (networkSocket) {
          networkSocket.removeAllListeners();
          networkSocket.disconnect();
        }
    }
   
    function getData() {
        console.log(className,'getData',data);
        return data;
    }

    function setSuggestedNetwork(ssid) {
      data.suggestedNetwork = ssid;
      dataService.save("suggestedNetwork",ssid);
    }

    function joinNetwork(ssid,password) {
        dataService.save("ssid",ssid);

        var url = data.url || socketAccessPointURL;

        connectTo(url + "/network", function(err,socket) {

            socket.emit('joinNetwork', {ssid:ssid, passphrase:password}, function(err,res) {
                //this callback will never be called because the accesspoint disappears directly after join
            });
        });
    }

    return {
        connect: connect,
        disconnect: disconnect,
        joinNetwork: joinNetwork,
        getData: getData
    }

});
