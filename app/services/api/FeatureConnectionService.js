app.service('FeatureConnectionService', function(socketFactory,$rootScope,PrinterDataService) {

  var className = 'FeatureConnectionService';

  function connect(printer,featureName,cb) {
    assert(arguments,['object','string']);
    //if printer object contains explicit offline value
    if (printer.online==false) {
      console.log(className,'printer offline');
      if (cb) cb();
      return;
    }

    //if socketFactory fires InvalidNamespace then set 'online:false' explicitly
    $rootScope.$on('InvalidNamespace', function(event, data) {
      PrinterDataService.updatePrinterData(printer,{online:false});
      console.error(className,'InvalidNamespace handler',data,printer);
    })

    $rootScope.$on('PrinterOnline', function(event,data) {
      //console.info(className,'PrinterOnline',data,data==printer);

      if (data==printer) {
        var socketName = makeSocketName(printer,featureName);
        var socket = socketFactory.getSocket(socketName);
        if (!socket) connectSocket(printer,featureName,cb);
      }
    });

    $rootScope.$on('PrinterOffline', function(event,data) {
      //
    });

    connectSocket(printer,featureName,cb)
  }

  function connectSocket(printer,featureName,cb) {
    assert(arguments,['object','string']);
    var socketName = makeSocketName(printer,featureName);
    socketFactory.connect(socketName,function(socket) {

      //console.log(className,'connected',socketName);
      
      if (!printer.online) {
        console.log(className,'connectSocket: connected with socket but offline')
      }

      if (cb) cb(socket);
    })
  }

  function disconnect(printer,featureName,cb) {
    assert(arguments,['object','string']);
    var socketName = makeSocketName(printer,featureName);
    var socket = socketFactory.getSocket(socketName);

    console.log(className,'disconnect',socketName);

    if (socket) {
      //WAS: socketFactory.disconnect(socket);
      socketFactory.disconnect(socketName);
    }

    if (cb) cb();
  }

  function makeSocketName(printer,featureName) {
    assert(arguments,['object','string']);
    return printer.id + (featureName ? '-' : '') + featureName;
  } 

  function getSocket(printer,featureName) {
    assert(arguments,['object','string']);
    var socketName = makeSocketName(printer,featureName);
    return socketFactory.getSocket(socketName);
  }

  function emit(printer,featureName,eventName,params,cb) {
    assert(arguments,['object','string','string']);
    var socket = getSocket(printer,featureName);
    if (!socket) console.error(className,'emit','socket undefined',printer,featureName);
    else socket.emit(eventName,params,cb);
    console.info(className,'emit',printer.id,featureName,eventName,params);

  }

  function addListeners(printer,featureName,eventNames) {
    assert(arguments,['object','string','object']);
    for (var i in eventNames) {
      addListener(printer,featureName,eventNames[i]);
    }
  }

  function addListener(printer,featureName,eventName) {
    assert(arguments,['object','string','string']);
    getSocket(printer,featureName).on(eventName, function(data) {
      // console.log(className,'socket.on',featureName || "root",eventName);
      var obj={}; 
      obj[featureName || "root"] = {}; 
      obj[featureName || "root"][eventName]=data;
      PrinterDataService.updatePrinterData(printer,obj);
    });
  }

  return {
    connect: connect,
    disconnect: disconnect,
    getSocket: getSocket,
    addListener: addListener,
    addListeners: addListeners,
    emit: emit
  }

});
