app.service('InstalledPrintersService', function(socketFactory,$rootScope,dataService,PrinterDataService) {

  var className = 'InstalledPrintersService';
  var printers = []; //array with references to printers object
  var connected = false;

  function connect(cb) {
    if (connected) {
      console.log(className,'already connected');
      if (cb) cb();
    } else {
      socketFactory.connect('printers',function(socket) {
        console.log(className,'connected',socket);
        addEventListeners(socket)
        connected = true;
        if (cb) cb();
      })
    }
  }

  function disconnect(cb) {
    console.error(className,'dont disconnect me');
  }

  function addEventListeners(socket) { 
    console.log(className,'addEventListeners');
    
    socket
    .on('list',onList)
    .on('changed',onChanged)
    .on('appeared',onAppeared)
    .on('disappeared',onDisappeared)
  }

  function onList(data) {
    console.log(className,'onList',data);
    // callback('onList',printers);
  }

  function onChanged(data) {
    console.log(className,'onChanged',data);
    var printer = PrinterDataService.getPrinterById(data.id);
    var onlineStateChanged = (printer.online!=data.online);
    PrinterDataService.updatePrinterData(printer,data);

    console.log(className,'onlineStateChanged',onlineStateChanged);

    if (printer.online) {
      console.log(className,'broadcast PrinterOnline')
      $rootScope.$broadcast('PrinterOnline',printer);
    }
  }

  function onAppeared(data) {
    console.log(className,'onAppeared',data);
    var printer = PrinterDataService.getPrinterById(data.id);
    if (printers.indexOf(printer)==-1) printers.push(printer);
    PrinterDataService.updatePrinterData(printer,data);
    PrinterDataService.updatePrinterData(printer,{installed:true});

    if (printer.online) {
      console.log(className,'broadcast PrinterOnline')
      $rootScope.$broadcast('PrinterOnline',printer);
    }
  }

  function onDisappeared(data) {
    console.log(className,'onDisappeared',data);
    var printer = PrinterDataService.getPrinterById(data.id);
    PrinterDataService.updatePrinterData(printer,data);
    PrinterDataService.updatePrinterData(printer,{installed:false});
    _removePrinter(printer); 
  }

  function _removePrinter(printer) {
    var index = printers.indexOf(printer);
    if (index!=-1) {
      printers.splice(index,1);
    }
  }

  function getPrinters() {
    return printers;
  }  

  function isNotConnected() {
    return !connected;
  }

  function install(printer,cb) {
    assert(arguments,['object']);

    var socket = socketFactory.getSocket('printers');
    if (!socket) return console.error(className,'install');

    socket.emit('add',{id:printer.id},function(err,data) {   
      console.log(className,'install cb',err,data);
      if (cb) cb();
    });
  }

  function uninstall(printer,cb) {
    assert(arguments,['object']);

    var socket = socketFactory.getSocket('printers');
    if (!socket) return console.error(className,'uninstall');

    socket.emit('remove',{id:printer.id},function(err,data) {   
      console.log(className,'uninstall cb',err,data);
      _removePrinter(printer);

      if (cb) cb();
    });
  }

  return {
    connect: connect,
    disconnect: disconnect,
    getPrinters: getPrinters, // returns list of InstalledPrinters (not to be confused with PrinterDataService.getPrinters which contains 'all' printers
    isNotConnected: isNotConnected,
    install: install,
    uninstall: uninstall
  }

});

