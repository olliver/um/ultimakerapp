app.service('LocalPrintersService', function(socketFactory, $rootScope, dataService, PrinterDataService) {

  var className = 'LocalPrintersService';
  var printers = [];
  var connected = false;

  function connect(cb) {
    if (connected) {
      console.log(className,'already connected');
      if (cb) cb();
    } else {
      socketFactory.connect('localprinters',function(socket) {
        console.log(className,'connected',socket);
        addEventListeners(socket)
        connected = true;
        if (cb) cb();
      })
    }
  }

  function disconnect(cb) {
    console.log(className,'disconnect from localprinters');
    socketFactory.disconnect('localprinters');
  }

  function addEventListeners(socket) { 
    console.log(className,'addEventListeners');
    
    socket
    .on('list',onList)
    // .on('changed',onChanged)
    .on('appeared',onAppeared)
    .on('disappeared',onDisappeared)
  }

  function onList(data) {
    console.log(className,'onList',data);
    // callback('onList',printers);
  }

  function onChanged(data) { //update printer data if there's a change
    console.log(className,'onChanged',data); 
    var printer = PrinterDataService.getPrinterById(data.id);
    PrinterDataService.updatePrinterData(printer,data);
  }

  function onAppeared(data) {
    console.log(className,'onAppeared',data);
    var printer = PrinterDataService.getPrinterById(data.id); //change in get printer by data (and combine it with 'update')
    if (printers.indexOf(printer)==-1) printers.push(printer);
    PrinterDataService.updatePrinterData(printer,data); //important if printer is not in InstalledPrinters yet or in some other cases
    PrinterDataService.updatePrinterData(printer,{isLocal:true});
  }

  function onDisappeared(data) {
    console.log(className,'onDisappeared',data);
    var printer = PrinterDataService.getPrinterById(data.id);
    var index = printers.indexOf(printer);
    if (index!=-1) {
      printers.splice(index,1);
    }
    PrinterDataService.updatePrinterData(printer,data); //important to copy received data in 'new' printer object. also in onDisappeared
    PrinterDataService.updatePrinterData(printer,{isLocal:false});
  }

  function getPrinters() {
    return printers;
  }  

  function waitForPrinterByName(name,cb) {
    var timer = setInterval(function() {
      console.log('waiting for printer ('+name+') to appear');
      for (var i=0; i<printers.length; i++) {
        if (printers[i].name==name) {
          console.log("found printer",name);
          console.log("timeout is cleared now:",timer,"and callback is called for waitForPrinterByName",name);
          clearTimeout(timer);
          if (cb) cb(printers[i]);
          break;
        }
      } 
    },1000);
  }

  return {
    connect: connect,
    getPrinters: getPrinters,
    disconnect: disconnect,
    // getPrinterById: getPrinterById,
    // setCustomName: setCustomName,
    waitForPrinterByName: waitForPrinterByName
  }

});
