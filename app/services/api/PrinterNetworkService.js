app.service('PrinterNetworkService', function($rootScope,dataService,PrinterDataService,FeatureConnectionService) {

  var className = 'PrinterNetworkService';
  var featureName = 'network';

  function connect(printer,cb) {
    FeatureConnectionService.connect(printer,featureName,function(socket) {
      if (socket) addListeners(printer,socket);
      if (cb) cb();
    });
  }
  
  function disconnect(printer,cb) {
    FeatureConnectionService.disconnect(printer,featureName,cb);
  }

  function addListeners(printer,socket) {
    //no listeners yet needed.
    //var eventTypes = [];
    //FeatureConnectionService.addListeners(printer,featureName,eventTypes);
  }

  function forgetNetwork(printer,cb) {
    console.log(className,printer,'forgetNetwork');

    FeatureConnectionService.emit(printer,featureName,'forgetNetwork',{}, function(err,data) {
      //this callback won't be called since it is not send in time before disconnecting from the network
    });

    if (cb) cb();

  }

  return {
    connect: connect,
    disconnect: disconnect,
    forgetNetwork: forgetNetwork
  }

});