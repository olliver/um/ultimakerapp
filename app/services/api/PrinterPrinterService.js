app.service('PrinterPrinterService', function($rootScope,dataService,PrinterDataService,FeatureConnectionService) {
  var className = 'PrinterPrinterService';
  var featureName = 'printer';

  function connect(printer,cb) {
    FeatureConnectionService.connect(printer,featureName,function(socket) {
      if (socket) addListeners(printer,socket);
      if (cb) cb();
    });
  }
  
  function disconnect(printer,cb) {
    FeatureConnectionService.disconnect(printer,featureName,cb);
  }

  function addListeners(printer,socket) {
    var eventTypes = ['state','temperatures','progress']
    FeatureConnectionService.addListeners(printer,featureName,eventTypes);
  }

  function stopPrint(printer) {
    console.log(className,printer,'stop');

    FeatureConnectionService.emit(printer,featureName,'stop',{}, function(err,data) {
      console.log(className,'stop callback',err,data)
    });

  }

  function home(printer) {
    console.log(className,printer,'home');

    FeatureConnectionService.emit(printer,featureName,'homeHead',{}, function(err,data) {
      console.log(className,'home callback',err,data)
    });
  }

  function print(printer,settings,cb) {
    var params = {
      url: settings.url,
      filename: settings.url,
      contentType: 'stl',
      printerType: printer.type     //'ultimaker2' or 'ultimaker'
    }

    console.log(className,'print',params);

    FeatureConnectionService.emit(printer,featureName,'print',params,function(err,data) {
      console.log(className,'print callback',err,data)
      if (cb) cb();
    });
    
  }

  return {
    connect: connect,
    disconnect: disconnect,
    stopPrint: stopPrint,
    print: print,
    home: home,
  }

});

