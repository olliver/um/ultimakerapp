app.service('PrinterRootService', function(socketFactory,$rootScope,dataService,PrinterDataService,FeatureConnectionService) {

  var className = 'PrinterRootService';
  var featureName = '';

  function connect(printer,cb) {
    FeatureConnectionService.connect(printer,featureName,function(socket) {
      if (socket) addListeners(printer,socket);
      if (cb) cb();
    });
  }
  
  function disconnect(printer,cb) {
    FeatureConnectionService.disconnect(printer,featureName,cb);
  }

  function addListeners(printer,socket) {
    // var eventTypes = ['printerState','wifiSSID','networkAddress'];
    var eventTypes = ['online','name','printerState','wifiState','wifiSSID','hotspotSSID','hotspotState','ethernetState','ethernetAddress','networkState','networkAddress','slicerState'];
    FeatureConnectionService.addListeners(printer,featureName,eventTypes);
  }

  return {
    connect: connect,
    disconnect: disconnect
  }

});
