app.service('PrinterSlicerService', function($rootScope,FeatureConnectionService,PrinterDataService) {

  var className = 'PrinterSlicerService';
  var featureName = 'slicer';

  function connect(printer,cb) {
    FeatureConnectionService.connect(printer,featureName,function(socket) {
      if (socket) addListeners(printer,socket);
      if (cb) cb();
    });
  }
  
  function disconnect(printer,cb) {
    FeatureConnectionService.disconnect(printer,featureName,cb);
  }

  function addListeners(printer,socket) {
    var eventTypes = ['progress'];
    FeatureConnectionService.addListeners(printer,featureName,eventTypes);
  }

  return {
    connect: connect,
    disconnect: disconnect,
  }

});