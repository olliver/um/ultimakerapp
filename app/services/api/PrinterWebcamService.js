app.service('PrinterWebcamService', function($rootScope,dataService,FeatureConnectionService) {
  var className = 'PrinterWebcamService';
  var featureName = 'webcam';
  var imageSrc = '';

  function connect(printer,cb) {
    console.log(className,"connect");

    FeatureConnectionService.connect(printer,featureName,function(socket) {
      console.log(className,"on feature connect");
      if (socket) addListeners(printer,socket);
      if (cb) cb();
    });
  }
  
  function disconnect(printer,cb) {
    console.log(className,'disconnect');
    // ss(socket).off('image', onImage);
    FeatureConnectionService.disconnect(printer,featureName,cb);
    // ss(socket).removeAllListeners(); //check me
    // ss(socket).disconnectAll();
  }

  function onImage(stream) {
    console.log(className,"ss.onImage");

    //console.log('image with stream');
    var binaryString = "";

    stream.on('data', function(data) {
      console.log(className,'data');
      for(var i=0;i<data.length;i++) {
        binaryString+=String.fromCharCode(data[i]);
      }
    });

    stream.on('end', function() {
      imageSrc = "data:image/jpg;base64,"+window.btoa(binaryString);
      binaryString = "";
      $rootScope.$broadcast('onWebcamImage',imageSrc);
    });
  }

  function addListeners(printer,socket) {
    console.log(className,"addListeners",printer,socket);
    ss(socket).on('image', onImage);
  }

  return {
    connect: connect,
    disconnect: disconnect,
  }

});