app.factory('AuthFactory', function ($rootScope,socketserverURL,registerURL,$http) {
  var className = "AuthFactory";
  var validated = false;

  return {

    getKey: function() {
      return localStorage.getItem("clientKey") || "";
    },

    setKey: function(key) {
      localStorage.setItem("clientKey",key);   
    },

    validate: function(success_cb, error_cb) {
      var self = this;

      if (validated) {
        // console.log(className,'already validated')
        if (success_cb) success_cb();
        return;
      }

      var url = socketserverURL + "?key=" + self.getKey();
      console.log(className,url);

      var socket = io.connect(url, {forceNew:true});

      socket.on("connect_error", function(data) {
        console.log(className,'connect_error',url,data);
        $rootScope.$broadcast("onRootSocketConnectError",data);
      });

      socket.on("error", function(data) {
        console.log("error",data);

        if (data!="User unknown") { 
          error_cb();
          return; // only request a key when data=='User unknow' otherwise just call error_cb and return
        }

        $http.post(registerURL).success(function(data) {
          console.log("http success",data);
          var url = socketserverURL + "?key=" + data.key;
          self.setKey(data.key);
          console.log(className,"retry: ",url);

          var socket = io.connect(url, {forceNew:true});

          socket.on("error", function(data) {
            console.log("still wrong, fatal error",data); 
            if (error_cb) {
              error_cb();
            }
          });

          socket.on("connect", function() {
            console.log(className,"connect");
            validated = true;
            if (success_cb) {
              success_cb();
            }
          });

        });
      });

      socket.on("connect", function() {
        console.log(className,"on connect /",url);
        $rootScope.$broadcast("onRootSocketConnect");

        if (success_cb) {
          validated = true;
          success_cb();
        }
      });

    }

  };

});
