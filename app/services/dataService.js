'use strict'

app.service('dataService', function() {

    var savedData = {};
    
    function save(key,value) {
        set(key,value,true);
    }

    function set(key, value, saveInLocalStorage) {
        savedData[key] = value;
        if (saveInLocalStorage) {
            localStorage.setItem(key,value);
        }
    }
    
    function get(key) {
        var value = savedData[key];
        if (!value) {
            value = localStorage.getItem(key);
        }
        if (!value) {
            value = undefined;
        }
        return value;
    }
    
    function getCustomName(id,defaultValue) {
        if (id===undefined) {
            console.error("dataService.getCustomName: id undefined");
            return;
        }
        
        var customName = get(id+"customName");
        if (!customName) {
            customName = defaultValue || "?"; //get(id+"name");
        }
        return customName;
    }
    
    function setCustomName(id,name) {
        set(id+"customName",name,true);
        //console.log(id,name);
    }

    return {
        set: set,
        get: get,
        save: save,
        // load: load,  //just use get
        getCustomName: getCustomName,
        setCustomName: setCustomName
    };

});