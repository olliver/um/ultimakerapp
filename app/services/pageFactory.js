'use strict'

app.factory('Page', function(){
    var title = '';
    var displaybackbutton = false;
    var displayLogo = false;
    var displayPrinterSettings = false;

    return {
        title: function() { return title; },
        setTitle: function(newTitle) { title = newTitle; },
        setLogo: function(showhide) { if(showhide){displayLogo = true} else {displayLogo = false}; },
        logo: function() { return displayLogo; /*if(displayLogo){return false} else {return true};*/ },
        backbutton: function() { return displaybackbutton; },
        setBackbutton: function(showhide) { if(showhide){displaybackbutton =true} else {displaybackbutton = false}; },
        goBack: function() { window.history.back(); },
        settingsbutton: function() { return displayPrinterSettings; },
        setSettingsbutton: function(showhide) { displayPrinterSettings = showhide; }
    };
});