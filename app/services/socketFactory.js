app.factory('socketFactory', ['$rootScope', 'socketserverURL', function ($rootScope,socketserverURL,AuthFactory) {
  //TODO rename to service

  var className = 'socketFactory';
  var sockets = {};

  function getCompleteURL(url) {
    assert(arguments,['string']);

    //when url is relative add socketserverURL as a prefix
    var r = new RegExp('^(?:[a-z]+:)?//', 'i');
    if (!r.test(url)) { 
      url = socketserverURL + '/' + url;
    }

    console.log(className,"AuthFactory",AuthFactory);

    //add client key
    var clientKey = localStorage.getItem("clientKey") || ""; ////somehow we cannot use AuthFactory.getKey here.
    if (url.indexOf('?key=')==-1) url += '?key=' + clientKey; //make sure not to add key multiple times

    return url;
  }

  function connect(url,cb) {
    assert(arguments,['string']);
    // console.log(className,'connect',url);

    url = getCompleteURL(url);

    //if socket already exists do cb()
    if (sockets[url]) {
      console.error(className,'connect: already connected to',url,'- not calling cb');
      // if (cb) cb(sockets[url]);
    } 

    //otherwise connect and then cb
    else {
      console.log(className,'connect:',url);

      sockets[url] = io.connect(url, {autoConnect:false}); //forceNew:true});
      
      if (sockets[url].connected) {
        if (cb) cb(sockets[url]);
        return;
      }

      sockets[url].connect();

      var eventTypes = ['message', 'connect', 'connecting', 'disconnect', 'connect_failed', 'connect_timeout',
                          'connect_error', 'error', 'reconnect_failed', 'reconnect', 'reconnecting', 'data'];

      function addListener(eventType) {
        sockets[url].on(eventType, function(data) {
            console.log(className,'[event]', eventType, url, data); //, sockets[url]);
        });
      }

      for(var i in eventTypes) {
          addListener(eventTypes[i]);
      }

      sockets[url].once('connect', function() {
        //FIXME now it's time to remove the following listeners: connect, error and connect_error
        //https://github.com/Doodle3D/HostModule/blob/connman-simplified/lib/socket.io/Client.js#L199

        // console.log(className,'connect: connected:',url);
        if (cb) cb(sockets[url]); 
      });

      sockets[url].once('error', function(data) {
        //FIXME now it's time to remove the following listeners: connect, error and connect_error
        //https://github.com/Doodle3D/HostModule/blob/connman-simplified/lib/socket.io/Client.js#L199

        // console.log(className,'connect: error:',url,data);
        if (data=="Invalid namespace") {
          $rootScope.$broadcast('InvalidNamespace',sockets[url]);
          disconnect(url);
        }
      });

      sockets[url].once('connect_error', function(data) {
        //FIXME now it's time to remove the following listeners: connect, error and connect_error
        //https://github.com/Doodle3D/HostModule/blob/connman-simplified/lib/socket.io/Client.js#L199
      });

    }

    return sockets[url];
  }

  function disconnect(url) {
    assert(arguments,['string']);

    url = getCompleteURL(url);

    if (!sockets[url]) {
      console.error(className,'disconnect: not connected to',url);
    } else {
      console.log(className,'disconnecting',url)
      sockets[url].removeAllListeners();
      sockets[url].disconnect();
      delete sockets[url];
    }

  }

  function disconnectAll() { //all except root socket??? we don't want to handshake again

    for (var url in sockets) {
      if (url!=getCompleteURL('printers')) { //don't close InstalledPrinters socket
        disconnect(url);
      }
    }
  }

  function getSocket(url) {
    assert(arguments,['string']);

    var url = getCompleteURL(url);
    // console.log(className,'getSocket',url,sockets[url]);
    return sockets[url];
  }

  return {
    connect: connect,
    disconnect: disconnect,
    getSocket: getSocket,
    disconnectAll: disconnectAll
  }

}]);
