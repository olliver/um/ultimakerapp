var gulp = require('gulp'),
    livereload = require('gulp-livereload'),
    flatten = require('gulp-flatten'),
    concat = require('gulp-concat'),
    logger = require('gulp-logger'),
    sass = require('gulp-sass'),
    del = require('del'),
    colors = require('colors'),
    exec = require('child_process').exec;

var plugins = [ 
  "org.apache.cordova.device",
  "org.apache.cordova.inappbrowser",
  "org.apache.cordova.console",
  "org.apache.cordova.splashscreen",
  "org.apache.cordova.statusbar",
  "https://github.com/EddyVerbruggen/iOSWebViewColor-PhoneGap-Plugin.git",
  "https://github.com/companje/org.apache.cordova.wifiinfo.git",
  "https://github.com/companje/cordova-plugin-ZeroConf.git"
];

var paths = {
  components: [
    'bower_components/socket.io-client/socket.io.js',
    'bower_components/angular/angular.js',
    'bower_components/angular-touch/angular-touch.js',
    'bower_components/angular-resource/angular-resource.js',
    'bower_components/angular-animate/angular-animate.js',
    'bower_components/angular-route/angular-route.js',
    'bower_components/angular-scroll/angular-scroll.js',
    'lib/modernizr.custom.72484.js',
    'bower_components/jquery/dist/jquery.js',
    'bower_components/foundation/js/foundation.js',
    'bower_components/foundation/js/foundation.dropdown.js',
    'lib/socket.io-stream.js'
  ],
  controllers: [
  'app/controllers/**/*.js'
  ],
  services: [
  'app/services/**/*.js'
  ],
  directives: [
  'app/directives/*.js'
  ],
  sourcemaps: [
  'bower_components/**/*.min.js.map'
  ],
  styles: [
  'scss/*.scss'
  ],
  fonts: [
  'assets/fonts/*.ttf',
  'assets/fonts/*.woff',
  'assets/fonts/*.eot'
  ],
  appjs: [
  'app/app.js'
  ],
  appindex: [
  'app/index.html'
  ],
  configxml: [
  'settings/config.xml'
  ],
  views: [
  'app/views/**/*.html'
  ],
  images: [
  'assets/images/*.jpg',
  'assets/images/*.gif',
  'assets/images/*.png',
  'assets/images/*.ico',
  'assets/images/*.stl'
  ],
  plist: [
  'settings/Ultimaker-Info.plist'
  ],
  manifest: [
  'settings/AndroidManifest.xml'
  ],
  ultiviewer: [
  'lib/ultiviewer/*'
  ],
  icons: [
  'assets/icons/*.svg'
  ]

};
var allPaths = [];
for(var i in paths) {
  allPaths = allPaths.concat(paths[i]);
}

gulp.task('sass', function () {
    gulp.src('scss/app.scss') //paths.styles)
.pipe(sass({errLogToConsole: true}))
.pipe(gulp.dest('www/css'));
});

gulp.task('fonts', function () {
  gulp.src(paths.fonts)
  .pipe(gulp.dest('www/css/fonts'));
});

gulp.task('components', function() {
  gulp.src(paths.components)
  .pipe(concat('components.js'))
  .pipe(gulp.dest('www/js'));
})

gulp.task('controllers', function() {
  gulp.src(paths.controllers)
  .pipe(concat('controllers.js'))
  .pipe(gulp.dest('www/js'));
})

gulp.task('services', function() {
  gulp.src(paths.services)
  .pipe(concat('services.js'))
  .pipe(gulp.dest('www/js'));
})

gulp.task('directives', function() {
  gulp.src(paths.directives)
  .pipe(concat('directives.js'))
  .pipe(gulp.dest('www/js'));
})

gulp.task('copywww', function() {
  gulp.src('www/**')
  .pipe(gulp.dest('platforms/ios/www'));
})

gulp.task('ultiviewer', function() {
  gulp.src(paths.ultiviewer)
  .pipe(gulp.dest('www/ultiviewer'));
})

gulp.task('approot', function() {
  gulp.src(paths.appjs).pipe(gulp.dest('www/js'));
  gulp.src(paths.appindex).pipe(gulp.dest('www'));
  gulp.src(paths.views).pipe(gulp.dest('www/views'));
  gulp.src(paths.images).pipe(gulp.dest('www/images'));
})

gulp.task('settings', function() {
  // gulp.src(paths.configxml).pipe(gulp.dest('www'));
  // gulp.src(paths.configxml).pipe(gulp.dest('platforms/ios/Ultimaker'));
  gulp.src(paths.plist).pipe(gulp.dest('platforms/ios/Ultimaker'));
  gulp.src(paths.manifest).pipe(gulp.dest('platforms/android'));
})

gulp.task('plugins', function(cb) {
  exec("cordova plugin add " + plugins.join(' '), function(error, stdout, stderr) {
    if (stdout) console.log(stdout);
    if (stderr) console.log(stderr.red);
    if (error !== null) console.log(error.red);
    if (cb) cb();
  });
});

gulp.task('all', ['settings', 'approot', 'sass','controllers','services','directives','copywww', 'fonts', 'components', 'ultiviewer']);

gulp.task('watch', function() {
  gulp.watch(allPaths,['all']);
});

gulp.task('default', ['all','watch']);

gulp.task('clean', function(cb) {
  del(['www/*','plugins/*','platforms/*'],cb);
});


